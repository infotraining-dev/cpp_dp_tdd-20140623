#ifndef CHAIN_HPP_
#define CHAIN_HPP_

#include <iostream>
#include <string>

// "Handler"
class Handler
{
protected:
	Handler* successor_;
public:
	Handler() : successor_(0) {}

	void set_successor(Handler* successor)
	{
		successor_ = successor;
	}

    void handle(int request)
    {
        if (is_match(request))
            handle_request(request);
        else if (successor_)
            successor_->handle(request);
    }

	virtual ~Handler() {}
protected:
    virtual bool is_match(int arg) = 0;
    virtual void handle_request(int request) = 0;
};

// "ConcreteHandler1"
class ConcreteHandler1 : public Handler
{
protected:
    bool is_match(int arg)
    {
        return ( arg >= 0 ) && ( arg < 10 );
    }

	void handle_request(int request)
	{
        std::cout << "ConcreteHandler1 handled request " << request << std::endl;
	}

};

// "ConcreteHandler2"
class ConcreteHandler2 : public Handler
{
protected:
    bool is_match(int arg)
    {
        return ( arg >= 10 ) && ( arg < 20 );
    }

	void handle_request(int request)
	{
			std::cout << "ConcreteHandler2 handled request " << request << std::endl;
	}

};

// "ConcreteHandler3"
class ConcreteHandler3 : public Handler
{
protected:
    bool is_match(int arg)
    {
        return ( arg >= 20 ) && ( arg < 30 );
    }

	void handle_request(int request)
	{
        std::cout << "ConcreteHandler3 handled request " << request << std::endl;
	}
};

#endif /*CHAIN_HPP_*/
