#ifndef CHAIN_HPP_
#define CHAIN_HPP_

#include <iostream>
#include <string>

// "Handler"
class Handler
{
protected:
	Handler* successor_;
public:
	Handler() : successor_(0) {}

	void set_successor(Handler* successor)
	{
		successor_ = successor;
	}

    void handle_request(int request)
    {
        if (!do_handle_request(request) && successor_)
            successor_->handle_request(request);
    }

    virtual ~Handler() {}

private:
    virtual bool do_handle_request(int request) = 0;


};

// "ConcreteHandler1"
class ConcreteHandler1 : public Handler
{
public:
    bool do_handle_request(int request)
	{
		if (( request >= 0 ) && ( request < 10 ))
        {
			std::cout << "ConcreteHandler1 handled request " << request << std::endl;
           return true;
        }

        return false;
	}

};

// "ConcreteHandler2"
class ConcreteHandler2 : public Handler
{
public:
    bool do_handle_request(int request)
	{
        if (( request >= 10 ) && ( request < 20 ))
        {
			std::cout << "ConcreteHandler2 handled request " << request << std::endl;
            return true;
        }

        return false;
	}

};

// "ConcreteHandler3"
class ConcreteHandler3 : public Handler
{
public:
    bool do_handle_request(int request)
	{
		if (( request >= 20 ) && ( request < 30 ))
        {
			std::cout << "ConcreteHandler3 handled request " << request << std::endl;
            return true;
        }

        return false;
	}
};

#endif /*CHAIN_HPP_*/
