#include "mediator.hpp"

using namespace std;

int main()
{
	ConcreteMediator* m = new ConcreteMediator();

    ConcreteColleague1* c1 = new ConcreteColleague1(m);
    ConcreteColleague2* c2 = new ConcreteColleague2(m);

    m->set_colleague1(c1);
    m->set_colleague2(c2);

    c1->send("How are you?");
    c2->send("Fine, thanks");

    delete c1;
    delete c2;
    delete m;
}
