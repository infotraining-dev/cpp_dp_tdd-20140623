#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

	// rejestracja inwestor�w zainteresowanych powiadomieniami o zmianach kursu sp�ek
	// TODO:
    {
        Investor kulczyk("Kulczyk");
        Investor solorz("Solorz");

        misys.attach(&kulczyk);
        ibm.attach(&kulczyk);
        tpsa.attach(&solorz);

        // zmian kurs�w
        misys.set_price(360.0);
        ibm.set_price(210.0);
        tpsa.set_price(45.0);

        misys.detach(&kulczyk);

        misys.set_price(380.0);
        ibm.set_price(230.0);
        tpsa.set_price(15.0);
    }

    ibm.set_price(90.0);
}
