#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <set>

void warning_with_auto(const std::set<std::string>& container)
{
    for(const auto& s : container) // ref
        std::cout << s;
}

class Stock;

class Observer
{
public:
    virtual void update(Stock* stock) = 0;
    virtual ~Observer()
    {
    }
};

// Subject
class Stock
{
private:
	std::string symbol_;
	double price_;
    std::set<Observer*> observers_;
protected:
    void notify()
    {
        for(auto o : observers_)
            o->update(this);
    }

public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{

	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

	// TODO: rejestracja obserwatora
    void attach(Observer* o)
    {
        observers_.insert(o);
    }

	// TODO: wyrejestrowanie obserwatora
    void detach(Observer* o)
    {
        observers_.erase(o);
    }

	void set_price(double price)
	{
        if (price != price_)
        {
            price_ = price;
            notify();
        }

		// TODO: powiadomienie inwestorow o zmianie kursu...
	}
};

class Investor : public Observer
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

    void update(Stock* s)
	{
		// TODO: implementacja
        std::cout << "Investor notified:"
                  << " Symbol: " << s->get_symbol()
                  << " New price: " << s->get_price() << std::endl;
	}
};

#endif /*STOCK_HPP_*/
