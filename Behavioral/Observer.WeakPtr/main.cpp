#include <iostream>
#include <set>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/make_shared.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <cassert>
#include <cstdlib>
#include <stdexcept>

class Observer
{
public:
    virtual void update(const std::string& event_args) = 0;
    virtual ~Observer() {}
};

class Subject
{
    int state_;
    std::set<boost::weak_ptr<Observer>> observers_;

public:
    Subject() : state_(0)
    {}

    void register_observer(boost::shared_ptr<Observer> observer)
    {
        observers_.insert(observer);
    }

    void unregister_observer(boost::shared_ptr<Observer> observer)
    {
        observers_.erase(observer);
    }

    void set_state(int new_state)
    {
        if (state_ != new_state)
        {
            state_ = new_state;
            notify("Changed state on: " + boost::lexical_cast<std::string>(state_));
        }
    }

protected:
    void notify(const std::string& event_args)
    {
        std::set<boost::weak_ptr<Observer>>::iterator it = observers_.begin();

        while (it != observers_.end())
        {
            boost::shared_ptr<Observer> observer = it->lock();

            if (observer)
            {
                observer->update(event_args);
                ++it;
            }
            else
            {
                it = observers_.erase(it);
            }
        }
    }
};

class ConcreteObserver1 : public Observer
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver1: " << event << std::endl;
    }
};

class ConcreteObserver2
        : public Observer,
          public boost::enable_shared_from_this<ConcreteObserver2>
{
public:
    virtual void update(const std::string& event)
    {
        std::cout << "ConcreteObserver2: " << event << std::endl;
    }

    void register_me(Subject& s)
    {
        s.register_observer(boost::shared_ptr<Observer>(shared_from_this()));
    }
};

int main(int argc, char const *argv[])
{
    using namespace std;

    Subject s;

    boost::shared_ptr<Observer> o1 =
            boost::make_shared<ConcreteObserver1>();
    s.register_observer(o1);

    {
        boost::shared_ptr<ConcreteObserver2> o2 =
                boost::make_shared<ConcreteObserver2>();

        //s.register_observer(o2);
        o2->register_me(s);

        s.set_state(1);

        cout << "End of scope." << endl;
    }

    s.set_state(2);
}
