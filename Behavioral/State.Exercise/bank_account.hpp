#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>

//enum AccountState { OVERDRAFT, NORMAL };

class AccountContext
{
    int id_;
    double balance_;
public:
    AccountContext(int id, double balance = 0.0) : id_(id), balance_(balance)
    {}

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        return balance_;
    }

    void set_balance(double balance)
    {
        balance_ = balance;
    }
};

class AccountState
{
public:
    virtual void withdraw(double amount, AccountContext& context) const = 0;
    virtual void deposit(double amount, AccountContext& context) const = 0;
    virtual void pay_interest(AccountContext& context) const = 0;
    virtual void print_status(const AccountContext& context) const = 0;
    virtual ~AccountState() {}
};

class AccountStateBase : public AccountState
{
protected:
    double interest_rate_;
public:
    AccountStateBase(double interest_rate) : interest_rate_(interest_rate) {}

    virtual void deposit(double amount, AccountContext &context) const
    {
        context.set_balance(context.balance() + amount);
    }

    virtual void pay_interest(AccountContext& context) const
    {
        double interest = context.balance() + context.balance() * interest_rate_;
        context.set_balance(interest);
    }
};

class NormalState : public AccountStateBase
{

public:
    NormalState(double interest_rate) : AccountStateBase(interest_rate)
    {}

    void withdraw(double amount, AccountContext& context) const
    {
        context.set_balance(context.balance() - amount);
    }

    void print_status(const AccountContext& context) const
    {
        std::cout << "BankAccount#" << context.id()
                  << " Balance: " << context.balance()
                  << " State: Normal\n";
    }
};

class OverdraftState : public AccountStateBase
{

public:
    OverdraftState(double interest_rate) : AccountStateBase(interest_rate)
    {}

    void withdraw(double amount, AccountContext& context) const
    {
        std::cout << "Brak srodkow na koncie #" << context.id() << std::endl;
    }

    void print_status(const AccountContext& context) const
    {
        std::cout << "BankAccount#" << context.id()
                  << " Balance: " << context.balance()
                  << " State: Overdraft\n";
    }
};

class BankAccount
{
    AccountContext context_;
    const AccountState* state_;

    static NormalState NORMAL;
    static OverdraftState OVERDRAFT;
protected:
	void update_account_state()
	{
        if (context_.balance() < 0)
            state_ = &OVERDRAFT;
		else
            state_ = &NORMAL;
	}
public:
    BankAccount(int id) : context_(id), state_(&NORMAL) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_->withdraw(amount, context_);

        update_account_state();
	}

	void deposit(double amount)
	{
		assert(amount > 0);

        state_->deposit(amount, context_);

		update_account_state();
	}

	void pay_interest()
	{
        state_->pay_interest(context_);
	}

	void print_status() const
	{
        state_->print_status(context_);
	}

	double balance() const
	{
        return context_.balance();
	}

	int id() const
	{
        return context_.id();
	}
};

#endif
