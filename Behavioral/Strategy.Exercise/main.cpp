#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

typedef std::vector<StatResult> Results;

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data,
                           Results& results) = 0;
    virtual ~Statistics() {}
};

class Avg : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

class MinMax : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        auto minmax = std::minmax_element(data.begin(), data.end());

        results.push_back(StatResult("MIN", *minmax.first));
        results.push_back(StatResult("MAX", *minmax.second));
    }
};

class Sum : public Statistics
{
public:
    void calculate(const std::vector<double> &data, Results &results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.push_back(StatResult("SUM", sum));
    }
};

class StatGroup : public Statistics
{
    std::vector<std::shared_ptr<Statistics>> stats_;
public:
    void add(std::shared_ptr<Statistics> stat)
    {
        stats_.push_back(stat);
    }

    void calculate(const std::vector<double> &data, Results &results)
    {
        for(auto stat : stats_)
            stat->calculate(data, results);
    }
};

class DataAnalyzer
{
    std::shared_ptr<Statistics> statistics_;
    std::vector<double> data_;
public:
    DataAnalyzer(std::shared_ptr<Statistics> statistics)
        : statistics_(statistics)
    {
    }

    void load_data(const std::string& file_name)
    {
        data_.clear();

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data_.push_back(d);
        }

        std::cout << "File " << file_name << " has been loaded...\n";
    }

    void save_data(const std::string& file_name) const
    {
        std::ofstream fout(file_name.c_str());
        if (!fout)
            throw std::runtime_error("File not opened");

        for(std::vector<double>::const_iterator it = data_.begin(); it != data_.end(); ++it)
            fout << (*it) << std::endl;
    }

    void set_statistics(std::shared_ptr<Statistics> statistics)
    {
        statistics_ = statistics;
    }

    void calculate(Results& results)
    {
        statistics_->calculate(data_, results);
    }
};

void print_results(const Results& results)
{
    for(Results::const_iterator it = results.begin(); it != results.end(); ++it)
		std::cout << it->description << " = " << it->value << std::endl;
}

int main()
{
	Results results;

    std::shared_ptr<Statistics> avg(new Avg());
    std::shared_ptr<Statistics> minmax(new MinMax());
    std::shared_ptr<Statistics> sum(new Sum());

    std::shared_ptr<StatGroup> std_stats(new StatGroup());
    std_stats->add(avg);
    std_stats->add(minmax);
    std_stats->add(sum);

    DataAnalyzer da(std_stats);
	da.load_data("data.dat");

    da.calculate(results);

    print_results(results);

	std::cout << "\n\n";

	results.clear();
	da.load_data("new_data.dat");
	da.calculate(results);

    print_results(results);
}
