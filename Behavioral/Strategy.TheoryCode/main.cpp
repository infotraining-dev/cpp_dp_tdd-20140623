#include "strategy.hpp"

int main()
{
	Context* context;

	// Three contexts following different strategies
	Strategy* s1 = new ConcreteStrategyA();
	context = new Context(s1);
	context->context_interface();

	Strategy* s2 = new ConcreteStrategyB();
	context->reset_strategy(s2);
	context->context_interface();

	Strategy* s3 = new ConcreteStrategyC();
	context->reset_strategy(s3);
	context->context_interface();

	delete context;
}
