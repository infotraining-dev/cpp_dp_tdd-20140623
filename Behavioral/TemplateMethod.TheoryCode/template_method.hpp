#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>

class ProductA
{
public:
    virtual void run()
    {
        std::cout << "ProductA::run()" << std::endl;
    }
    virtual ~ProductA() {}
};

class ProductB : public ProductA
{
public:
    virtual void run()
    {
        std::cout << "ProductB::run()" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;

    virtual bool is_valid() const
    {
        // check
        return true;
    }

    virtual ProductA* create_product()
    {
        return new ProductA();
    }

public:
	void template_method()
	{
		primitive_operation_1();
        ProductA* product = create_product();
        product->run();
        delete product;

        if (is_valid())
            primitive_operation_2();
		std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    ProductA* create_product()
    {
        return new ProductB();
    }

    bool is_valid() const
    {
        return false;
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/
