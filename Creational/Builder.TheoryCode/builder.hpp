#ifndef BUILDER_HPP_
#define BUILDER_HPP_

#include <iostream>
#include <string>
#include <vector>

class Product;

// "Builder" 
class Builder
{
public:
	virtual void build_part_A() = 0;
	virtual void build_part_B() = 0;
	virtual Product* get_result() = 0;
	virtual ~Builder() {}
};


// "Director" 
class Director
{
public:
	void construct(Builder& builder)
	{
		builder.build_part_A();
		builder.build_part_B();
	}
};

// "Product" 
class Product
{
   std::vector<std::string> parts_;
public:
   void add(const std::string& part)
   {
	   parts_.push_back(part);
   }

   void show()
   {
	   for(size_t i = 0; i < parts_.size(); ++i)
		   std::cout << parts_[i] << std::endl;
   }
};


// "ConcreteBuilder1"
class ConcreteBuilder1 : public Builder
{
private:
	Product* product_;
public:
	ConcreteBuilder1() : product_(new Product()) 
	{
	}
	
	void build_part_A()
	{
		product_->add("PART A");
	}
	
	void build_part_B()
	{
		product_->add("PART B");
	}
	
	Product* get_result()
	{
		return product_;
	}
};

// "ConcreteBuilder2"
class ConcreteBuilder2 : public Builder
{
private:
	Product* product_;
public:
	ConcreteBuilder2() : product_(new Product()) 
	{
	}
	
	void build_part_A()
	{
		product_->add("PART X");
	}
	
	void build_part_B()
	{
		product_->add("PART Y");
	}
	
	Product* get_result()
	{
		return product_;
	}
};
     
#endif /*BUILDER_HPP_*/
