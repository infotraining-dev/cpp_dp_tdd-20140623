#include <vector>

#include "employee.hpp"
#include "hrinfo.hpp"

//HRInfo* gen_info(const Employee& e)
//{
//	if (const Salary* s = dynamic_cast<const Salary*>(&e))
//		return new StdInfo(s);
//	if (const Hourly* h = dynamic_cast<const Hourly*>(&e))
//		return new StdInfo(h);
//	if (const Temp* t = dynamic_cast<const Temp*>(&e))
//		return new TempInfo(t);
//	else
//		return 0;
//}

int main()
{
	using namespace std;

	vector<Employee*> emps;
	emps.push_back(new Salary("Jan Kowalski"));
	emps.push_back(new Hourly("Adam Nowak"));
	emps.push_back(new Temp("Anna Nowakowska"));

	cout << "HR Report:\n---------------\n";
	// generowanie obiektów typu HRInfo
	for(size_t i = 0; i < emps.size(); ++i)
	{
        std::unique_ptr<HRInfo> hri = emps[i]->create_hrinfo();

		cout << endl;
	} // wyciek pamięci

	// sprzątanie
	for(size_t i = 0; i < emps.size(); ++i)
		delete emps[i];
}
