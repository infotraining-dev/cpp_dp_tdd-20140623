/*
 * square.hpp
 *
 *  Created on: 04-02-2013
 *      Author: Krystian
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"
#include "rectangle.hpp"

// TODO: Doda� klase Square

namespace Drawing
{
    class Square  : public Shape
    {
        size_t size_;
        Drawing::Rectangle rect_;
    public:
        Square(int x = 0, int y = 0, int size = 0) : rect_(x, y, size, size)
        {
        }

        void draw() const
        {
            rect_.draw();
        }

        void move(int dx, int dy)
        {
            rect_.move(dx, dy);
        }

        void read(std::istream &in)
        {
            Point pt;
            size_t size;

            in >> pt;
            in >> size;

            rect_ = Rectangle(pt.x(), pt.y(), size, size);
        }

        void write(std::ostream& out)
        {
            out << "Square" << rect_.point() << " " << rect_.width() << "\n";
        }
    };
}

#endif /* SQUARE_HPP_ */
