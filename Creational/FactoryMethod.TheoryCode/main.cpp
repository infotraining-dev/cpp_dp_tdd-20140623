#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include "factory.hpp"

using namespace std;

class Client
{
private:
	vector<Product*> products_;
	Creator* creator_;
public:
	Client(Creator* creator) : creator_(creator)
	{ 
	}

	Client(const Client&);
	Client& operator=(const Client&);

	~Client()
	{
		vector<Product*>::const_iterator it = products_.begin();
		for( ; it != products_.end(); ++it)
			delete *it;
	}

	void init(size_t size)
	{
		for(size_t i = 0; i < size; ++i)
            products_.push_back(creator_->create_product());
	}

	void use()
	{
		cout << "Content of client:\n";
		vector<Product*>::const_iterator it = products_.begin();
		for( ; it != products_.end(); ++it)
			cout << (*it)->description() << endl;
	}
};

int main()
{
    typedef std::map<std::string, Creator*> Factory;
    Factory creators;
    creators.insert(std::make_pair("CreatorA", new ConcreteCreatorA()));
    creators.insert(std::make_pair("CreatorB", new ConcreteCreatorB()));
    creators.insert(std::make_pair("CreatorC", new ConcreteCreatorC()));

    Client client(creators["CreatorC"]);
	client.init(10);
	client.use();

    for(Factory::iterator it = creators.begin(); it != creators.end(); ++it)
        delete it->second;
}
