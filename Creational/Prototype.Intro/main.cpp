#include <iostream>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    Engine* clone() const
    {
        Engine* cloned_engine = do_clone();
        assert(typeid(*this) == typeid(*cloned_engine));

        return cloned_engine;
    }

    virtual ~Engine() {}
private:
    virtual Engine* do_clone() const = 0;
};

class Diesel : public Engine
{
public:
    virtual void start()
    {
        cout << "Diesel starts\n";
    }

    virtual void stop()
    {
        cout << "Diesel stops\n";
    }
private:
    Diesel* do_clone() const
    {
        return new Diesel(*this);
    }
};

class TDI : public Diesel
{
public:
    virtual void start()
    {
        cout << "TDI starts\n";
    }

    virtual void stop()
    {
        cout << "TDI stops\n";
    }
private:
    TDI* do_clone() const
    {
        return new TDI(*this);
    }
};

class Hybrid : public Engine
{
public:
    virtual void start()
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop()
    {
        cout << "Hybrid stops\n";
    }
private:
    Hybrid* do_clone() const override
    {
        return new Hybrid(*this);
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_(engine)
    {}

    Car(const Car& source) : engine_(source.engine_->clone())
    {
    }

    Car& operator=(const Car& c)
    {
        Engine* temp = c.engine_->clone();

        delete engine_;
        engine_ = temp;

        return *this;
    }

    ~Car() { delete engine_; }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\n";

    Car c2 = c1;

    c2.drive(300);
}

