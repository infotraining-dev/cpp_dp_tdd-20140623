#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <vector>
#include <functional>

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<Shape*> shapes_;
public:
    ShapeGroup() {}

    ShapeGroup(const ShapeGroup& source)
    {
        std::transform(source.shapes_.begin(), source.shapes_.end(),
                       std::back_inserter(shapes_), std::mem_fn(&Shape::clone));
    }

    void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), std::mem_fn(&Shape::draw));
    }

    void move(int dx, int dy)
    {
        using namespace std::placeholders;

        for(auto s : shapes_)
            s->move(dx, dy);
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream& in)
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string id;

            in >> id;

            Shape* shape = ShapeFactory::instance().create(id);
            shape->read(in);

            shapes_.push_back(shape);
        }
    }

    void write(std::ostream &out)
    {
        using namespace std::placeholders;
        std::for_each(shapes_.begin(), shapes_.end(),
                      std::bind(&Shape::write, _1, std::ref(out)));
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
