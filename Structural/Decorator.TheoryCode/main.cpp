#include "decorator.hpp"

void client(Component* c)
{
    c->operation();
}

int main()
{
	 // Create ConcreteComponent and two Decorators
	 ConcreteComponent* c = new ConcreteComponent();
	 ConcreteDecoratorA* d1 = new ConcreteDecoratorA(c);
	 Component* d2 = new ConcreteDecoratorB(d1);

     client(d2);

	 std::cout << std::endl;

	 delete d2;
}
