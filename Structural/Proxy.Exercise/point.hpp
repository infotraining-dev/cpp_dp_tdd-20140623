#ifndef POINT_HPP
#define POINT_HPP

#include <vector>
#include <iostream>
#include <stdexcept>

namespace Drawing
{

class Point
{
private:
	int x_;
	int y_;
public:
	Point(int x = 0, int y = 0) : x_(x), y_(y)
	{
	}

	int x() const
	{
		return x_;
	}

	void set_x(int x)
	{
		x_ = x;
	}

	int y() const
	{
		return y_;
	}

	void set_y(int y)
	{
		y_ = y;
	}

	void translate(int dx, int dy)
	{
		x_ += dx;
		y_ += dy;
	}

	friend std::ostream& operator<<(std::ostream& out, const Point& pt);
	friend std::istream& operator>>(std::istream& in, Point& pt);
};

inline
std::ostream& operator<<(std::ostream& out, const Point& pt)
{
	out << "[" << pt.x() << "," << pt.y() << "]";
	return out;
}

inline
std::istream& operator>>(std::istream& in, Point& pt)
{

	char start, separator, end;
	int x, y;

	if ( in >> start && start != '[')
	{
		in.unget();
		in.clear(std::ios_base::failbit);
		return in;
	}

	in >> x >> separator >> y >> end;

	if ( !in || separator != ',' || end != ']' )
		throw std::runtime_error("Stream reading error");

	pt.x_ = x;
	pt.y_ = y;

	return in;
}

}

#endif
