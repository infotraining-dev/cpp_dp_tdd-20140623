#include <iostream>
#include <string>
#include <map>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace ::testing;

class BowlingGame
{
    unsigned int rolls_[21] {};
    unsigned int current_roll_ {0};
    const unsigned int pins_in_frame = 10;
public:

    void roll(unsigned int pins)
    {
        rolls_[current_roll_++] = pins;
    }

    unsigned int score()
    {
        int result = 0;
        unsigned int index = 0;

        for(unsigned int frame_index = 0; frame_index < 10; ++frame_index)
        {
            if (is_strike(index))
            {
                result += pins_in_frame + strike_bonus(index);
                ++index;
            }
            else
            {
                result +=  sum_points_in_frame(index);

                if (is_spare(index))
                    result +=  spare_bonus(index);

                index += 2;
            }
        }
        return result;
    }
private:
    unsigned int sum_points_in_frame(unsigned int index)
    {
        return rolls_[index] + rolls_[index + 1];
    }

    bool is_strike(int index)
    {
        return rolls_[index] == pins_in_frame;
    }

    bool is_spare(unsigned int index)
    {
        return rolls_[index] + rolls_[index + 1] == pins_in_frame;
    }

    unsigned int spare_bonus(unsigned int index)
    {
       return rolls_[index + 2];
    }

    unsigned int strike_bonus(unsigned int index)
    {
        return rolls_[index + 1] + rolls_[index + 2];
    }
};

class BowlingGameTests : public Test
{
protected:
    BowlingGame game;

    void roll_many(unsigned int count, unsigned int pins)
    {
        for(int i = 0; i < count; ++i)
            game.roll(pins);
    }

    void roll_spare()
    {
        game.roll(5);
        game.roll(5);
    }

    void roll_strike()
    {
        game.roll(10);
    }
};

TEST_F(BowlingGameTests, WhenGameStartsScoreShouldBeZero)
{
    ASSERT_EQ(0, game.score());
}

struct GameCase
{
    unsigned int pins;
    unsigned int expected;
};

class BowlingGameTestsWithParams : public TestWithParam<GameCase>
{
protected:
    BowlingGame game;
};

TEST_P(BowlingGameTestsWithParams, WhenAllRollsWithoutMarkScoreShouldBeSumOfPins)
{
    GameCase input = GetParam();

    for(unsigned int i = 0; i < 20; ++i)
        game.roll(input.pins);

    ASSERT_THAT(game.score(), Eq(input.expected));
}

GameCase games[] = { GameCase { 1, 20} , GameCase { 2, 40}, GameCase { 3, 60 } };

INSTANTIATE_TEST_CASE_P(BulkTest, BowlingGameTestsWithParams, ValuesIn(games));

TEST_F(BowlingGameTests, WhenSpareNextRollIsCountedTwice)
{
    roll_spare();
    game.roll(3);

    roll_many(17, 0);

    ASSERT_THAT(game.score(), Eq(16));
}

TEST_F(BowlingGameTests, WhenStrikeTwoNextRollsAreCountedTwice)
{
    roll_strike();
    game.roll(3);
    game.roll(3);
    roll_many(16, 0);

    ASSERT_THAT(game.score(), Eq(22));
}

TEST_F(BowlingGameTests, WhenPerfectGameScoreShouldBe300)
{
    for(int i = 0; i < 12; ++i)
        roll_strike();

    ASSERT_THAT(game.score(), Eq(300));
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}

