#ifndef RECENTLY_USED_LIST_HPP
#define RECENTLY_USED_LIST_HPP

#include <string>
#include <deque>
#include <limits>
#include <algorithm>
#include <stdexcept>

class RecentlyUsedList
{
public:
    RecentlyUsedList(size_t capacity = std::numeric_limits<size_t>::max())
        : capacity_(capacity)
    {}

    bool empty() const
    {
        deque_.empty();
    }

    size_t size() const
    {
        return deque_.size();
    }

    void add(const std::string& item)
    {
        check_is_not_empty(item);

        auto duplicate_position = std::find(deque_.begin(), deque_.end(), item);

        if (duplicate_position != deque_.end())
            move_to_front(duplicate_position, item);
        else
        {
            deque_.push_front(item);
            trim_to_capacity();
        }
    }

    const std::string& front() const
    {
        return deque_.front();
    }

    const std::string& operator[](size_t index) const
    {
        return deque_[index];
    }

private:
    std::deque<std::string> deque_;
    size_t capacity_;

    template <typename Iter>
    void move_to_front(Iter duplicate_position, const std::string& item)
    {
        deque_.erase(duplicate_position);
        deque_.push_front(item);
    }

    void check_is_not_empty(const std::string& item)
    {
        if (item == "")
            throw std::invalid_argument("Empty std::string are not allowed");
    }

    void trim_to_capacity()
    {
        if (deque_.size() > capacity_)
            deque_.pop_back();
    }
};

#endif // RECENTLY_USED_LIST_HPP
