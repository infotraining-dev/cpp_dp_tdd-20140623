#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <deque>
#include "recently_used_list.hpp"

using namespace std;
using namespace ::testing;

class WhenUsingNew_RecentlyUsedList : public Test
{
protected:
    RecentlyUsedList list_;
    const string item_ = "item";
};

TEST_F(WhenUsingNew_RecentlyUsedList, IsEmptyAfterCreation)
{
    ASSERT_THAT(list_.empty(), Eq(true));
}

TEST_F(WhenUsingNew_RecentlyUsedList, IsNotEmptyAfterAddingAnItem)
{
    list_.add(item_);

    ASSERT_THAT(list_.empty(), Eq(false));
}

TEST_F(WhenUsingNew_RecentlyUsedList, ChangesSizeAfterAddingItem)
{
    ASSERT_THAT(list_.size(), Eq(0));

    list_.add(item_);

    ASSERT_THAT(list_.size(), Eq(1));
}

TEST_F(WhenUsingNew_RecentlyUsedList, LastInsertedItemIsInFront)
{
    list_.add(item_);

    ASSERT_THAT(list_.front(), Eq(item_));

    list_.add("item1");

    ASSERT_THAT(list_.front(), Eq(string("item1")));
}

TEST_F(WhenUsingNew_RecentlyUsedList, InsertingEmptyStringThrowsException)
{
    ASSERT_THROW(list_.add(""), invalid_argument);
}

class WhenUsingPopulated_RecentlyUsedList : public Test
{
protected:
    RecentlyUsedList list_;
    vector<string> items_ = {"item1", "item2", "item3" };

    virtual void SetUp() override
    {
        for(auto& item : items_)
            list_.add(item);
    }
};

TEST_F(WhenUsingPopulated_RecentlyUsedList, ItemsAreIndexedInLIFOOrder)
{
    ASSERT_THAT(list_[0], Eq(items_[2]));
    ASSERT_THAT(list_[1], Eq(items_[1]));
    ASSERT_THAT(list_[2], Eq(items_[0]));
}

TEST_F(WhenUsingPopulated_RecentlyUsedList, InsertingDuplicateDoesntChangeSize)
{
    size_t prev_size = list_.size();

    list_.add(items_[1]);

    ASSERT_THAT(list_.size(), Eq(prev_size));
}

TEST_F(WhenUsingPopulated_RecentlyUsedList, InsertingDuplicateMovesItemToFront)
{
    list_.add(items_[1]);

    ASSERT_THAT(list_[0], Eq(items_[1]));
    ASSERT_THAT(list_[1], Eq(items_[2]));
}

class WhenUsingBounded_RecentlyUsedList : public Test
{
protected:
    RecentlyUsedList list_ { 3 };
    vector<string> items_ = { "item1", "item2", "item3" };

    virtual void SetUp() override
    {
        for(auto& item : items_)
            list_.add(item);
    }
};

TEST_F(WhenUsingBounded_RecentlyUsedList, InsertingItemToFullListShouldNotChangeSize)
{
    size_t prev_size = list_.size();

    list_.add("item4");

    ASSERT_THAT(list_.size(), Eq(prev_size));
}

TEST_F(WhenUsingBounded_RecentlyUsedList, InsertingItemToFullListShouldTrimLastElement)
{
    list_.add("item4");

    ASSERT_THAT(list_[list_.size() - 1], Eq(items_[1]));
}
