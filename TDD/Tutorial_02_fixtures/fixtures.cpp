/*
 * simple.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Leszek
 */

#include <gtest/gtest.h>
#include <vector>
#include <iostream>

class ArrayTests: public ::testing::Test
{
protected:
    std::vector<int> arr1;
public:
    ArrayTests()
    {
        // constructor launched before EVERY test
        std::cout << "Inside fixture constructor" << std::endl;
        for (int i = 0; i < 5; ++i)
        {
            arr1.push_back(i + 1);
        }
    }
};

class SharedArrayTests: public ::testing::Test
{
protected:
    static std::vector<int> arr1;

    static void SetUpTestCase()
    {
        std::cout << "Inside static fixture constructor" << std::endl;
        for (int i = 0; i < 5; ++i)
        {
            arr1.push_back(i + 1);
        }
    }
};

std::vector<int> SharedArrayTests::arr1;

TEST_F(SharedArrayTests, ArrayTestFirst)
{
    EXPECT_EQ(arr1[0], 1);
}

TEST_F(SharedArrayTests, ArrayIsNotEmpty)
{
    EXPECT_FALSE(arr1.empty());
}

TEST_F(SharedArrayTests, ArrayTestSecond)
{
    EXPECT_EQ(arr1[1], 2);
}

TEST_F(ArrayTests, ArrayEquality)
{
    int arr2[5] =
    { 1, 2, 3, 2, 5 };
    for (int i = 0; i < 5; ++i)
    {
        // "pipe" additional information for test failure
        EXPECT_EQ(arr1[i], arr2[i]) << " for index: " << i;
    }
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

