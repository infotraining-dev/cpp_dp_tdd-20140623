/*
 * simple.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Leszek
 */

#include <gtest/gtest.h>

bool is_prime(unsigned long n)
{
    return true;
}

TEST(FloatEquality, ArrayEqualitySimple)
{
    float arr1[5] = {1, 1, 1, 1, 1};
    float arr2[5] = {1.0001, 1.0000001, 1.00000001, 1, 1};
    for (int i = 0; i < 5; ++i)
    {
        EXPECT_FLOAT_EQ(arr1[i], arr2[i]) << "for index: " << i;
    }
}

TEST(FloatEquality, MarginError)
{
    float a = 1;
    float b = 1.1;
    EXPECT_NEAR(a, b, 0.2);
}

TEST(FloatEquality, SimpleError)
{
    float a = 1.0;
    float b = 1.0 + 1e-7;
    EXPECT_FLOAT_EQ(a, b) << a << "=" << b;
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

