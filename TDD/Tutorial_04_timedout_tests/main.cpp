/*
 * simple.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Leszek
 */

#include <gtest/gtest.h>
#include <time.h>
#include <unistd.h>

int slow_func(int trigger)
{
    if (trigger == 44)
    {
        sleep(2);
        return trigger;
    }
    else
    {
        return trigger;
    }
}

class QuickTest: public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        start_time_ = time(NULL);
    }

    virtual void TearDown()
    {
        time_t end_time = time(NULL);
        EXPECT_TRUE(end_time - start_time_ <= 1) << " Test was too long";
    }

    time_t start_time_;
};

TEST_F(QuickTest, SimpleTest)
{
    EXPECT_EQ(1, slow_func(1));
    EXPECT_EQ(44, slow_func(44));
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

