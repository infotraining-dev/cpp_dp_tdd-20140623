/*
 * simple.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Leszek
 */

#include <gtest/gtest.h>

bool is_prime(unsigned long n)
{
    return true;
}

TEST(PrimeTest, SomeNumbersArePrimes)
{
    ASSERT_TRUE(is_prime(2));
}

TEST(SimpleTest, JustEqual)
{
    ASSERT_EQ(1, 1);
}

// You can disable failing test (TODO-sort-of)
// by adding DISABLED_ in front of the test name
TEST(ArrayEquality, DISABLED_ArrayEqualitySimple)
{
    int arr1[5] =
    { 1, 2, 3, 4, 5 };
    int arr2[5] =
    { 1, 2, 3, 2, 5 };
    for (int i = 0; i < 5; ++i)
    {
        // "pipe" additional information for test failure
        EXPECT_EQ(arr1[i], arr2[i]) << " for index: " << i;
    }
}

int main(int argc, char** argv)
{
    //::testing::GTEST_FLAG(filter) = "*Just*";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

