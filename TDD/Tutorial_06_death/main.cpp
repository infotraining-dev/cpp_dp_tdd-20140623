/*
 * simple.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Leszek
 */

#include <gtest/gtest.h>

bool is_prime(long n)
{
    if (n > 0)
        return true;
    else
    {
        std::cerr << "Error: Negative or zero input\n";
        exit(-1);
    }
}

TEST(PrimeTest, SomeNumbersArePrimes)
{
    ASSERT_TRUE(is_prime(2));
}

TEST(PrimeTest, PrimesForPositiveNumbers)
{
    ASSERT_EXIT(is_prime(-1), ::testing::ExitedWithCode(-1),
            "Error: Negative or zero input");
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

