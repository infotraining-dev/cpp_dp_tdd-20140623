/*
 * simple.cpp
 *
 *  Created on: 30-01-2013
 *      Author: Leszek
 */

#include <gtest/gtest.h>
#include <stdexcept>

void simple_crash()
{
	throw std::runtime_error("ERROR");
}

TEST(PrimeTest, SomeNumbersArePrimes)
{
	EXPECT_THROW(simple_crash(), std::runtime_error);
}


int main(int argc, char** argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

