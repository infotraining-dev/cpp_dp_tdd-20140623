/*
 * Point.h
 *
 *  Created on: 02-02-2013
 *      Author: Leszek
 */

#include "graphics.hpp"

#ifndef POINT_H_
#define POINT_H_

class Point {
public:
    Point();
    int x() const;
    int y() const;
    void draw(Graphics& e) const;
    virtual ~Point();
};

#endif /* POINT_H_ */
