/*
 * Point_test.cpp
 *
 *  Created on: 02-02-2013
 *      Author: Leszek
 */

#include "Point.h"
#include "graphics.hpp"
#include <gtest/gtest.h>

using namespace ::testing;

TEST(PointTests, WhenCreatedPointsToOrigin)
{
    Point pt;

    EXPECT_EQ(0, pt.x());
    EXPECT_EQ(0, pt.y());
}

TEST(PointTests, CanDraw)
{
    Point pt;

    MockGraphics engine;
    EXPECT_CALL(engine, draw_point(_, Gt(10)));

    pt.draw(engine);
}
