#ifndef GRAPHICS_H_
#define GRAPHICS_H_

class Graphics {
public:
    virtual void draw_point(int x, int y) = 0;
    virtual void draw_line(int x1, int y1, int x2, int y2) = 0;
    virtual void draw_ellipse(int x, int y, int width, int height) = 0;
    virtual void draw_rectangle(int x, int y, int width, int height) = 0;
    virtual ~Graphics() {};
};

#include <gmock/gmock.h>

class MockGraphics: public Graphics {
public:
    MOCK_METHOD2(draw_point,
            void(int x, int y));
    MOCK_METHOD4(draw_line,
            void(int x1, int y1, int x2, int y2));
    MOCK_METHOD4(draw_ellipse,
            void(int x, int y, int width, int height));
    MOCK_METHOD4(draw_rectangle,
            void(int x, int y, int width, int height));
};

#endif
