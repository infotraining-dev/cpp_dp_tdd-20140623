#include "warehouse_mock.hpp"

using namespace ::testing;

class OrderBehaviorTests : public Test
{
protected:
    const std::string TALISKER = "Talisker";

    WarehouseMock warehouse_;
};

TEST_F(OrderBehaviorTests, WhenEnoughItemsInStoreProductsShouldBeRemovedFromStock)
{
    size_t quantity = 50;
    Order order(TALISKER, quantity);

    EXPECT_CALL(warehouse_, has_inventory(TALISKER, Le(quantity))).WillOnce(Return(true));
    EXPECT_CALL(warehouse_, remove(TALISKER, quantity)).Times(1);

    order.fill(warehouse_);
}

TEST_F(OrderBehaviorTests, WhenEnoughItemsInStoreOrderShouldBeFilled)
{
    size_t quantity = 50;
    Order order(TALISKER, quantity);

    EXPECT_CALL(warehouse_, has_inventory(TALISKER, quantity)).WillOnce(Return(true));

    order.fill(warehouse_);

    ASSERT_TRUE(order.is_filled());
}

TEST_F(OrderBehaviorTests, WhenNotEnoughItemsInStoreProductsQuantityOnStockRemainsTheSame)
{
    size_t quantity = 50;
    Order order(TALISKER, quantity);

    EXPECT_CALL(warehouse_, has_inventory(TALISKER, _)).WillOnce(Return(false));
    EXPECT_CALL(warehouse_, remove(_, _)).Times(0);

    order.fill(warehouse_);
}

