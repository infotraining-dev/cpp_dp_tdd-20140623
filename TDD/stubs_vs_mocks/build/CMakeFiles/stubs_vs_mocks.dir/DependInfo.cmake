# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/developer/Szkolenie/CPP_DP_TDD 20140623/TDD/stubs_vs_mocks/behavior_tests.cpp" "/home/developer/Szkolenie/CPP_DP_TDD 20140623/TDD/stubs_vs_mocks/build/CMakeFiles/stubs_vs_mocks.dir/behavior_tests.cpp.o"
  "/home/developer/Szkolenie/CPP_DP_TDD 20140623/TDD/stubs_vs_mocks/main.cpp" "/home/developer/Szkolenie/CPP_DP_TDD 20140623/TDD/stubs_vs_mocks/build/CMakeFiles/stubs_vs_mocks.dir/main.cpp.o"
  "/home/developer/Szkolenie/CPP_DP_TDD 20140623/TDD/stubs_vs_mocks/state_tests.cpp" "/home/developer/Szkolenie/CPP_DP_TDD 20140623/TDD/stubs_vs_mocks/build/CMakeFiles/stubs_vs_mocks.dir/state_tests.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/developer/gmock-1.7.0/include"
  "/home/developer/gmock-1.7.0/gtest/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
