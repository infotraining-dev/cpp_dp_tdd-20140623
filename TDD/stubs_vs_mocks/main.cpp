#include <iostream>
#include <string>
#include <map>
#include "gmock/gmock.h"

int main(int argc, char** argv)
{
    ::testing::InitGoogleMock(&argc, argv);

    return RUN_ALL_TESTS();
}

