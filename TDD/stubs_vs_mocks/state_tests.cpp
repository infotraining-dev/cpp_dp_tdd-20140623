#include "warehouse_impl.hpp"

using namespace ::testing;

class OrderStateTests : public ::testing::Test
{
protected:
    const std::string TALISKER = "Talisker";
    WarehouseImpl warehouse_;

    void SetUp()
    {
        warehouse_.add(TALISKER, 50);
    }
};


TEST_F(OrderStateTests, WhenEnoughItemsInStoreOrderShouldBeFilled)
{
    Order order(TALISKER, 50);

    order.fill(warehouse_);

    ASSERT_TRUE(order.is_filled());
    ASSERT_THAT(warehouse_.get_inventory(TALISKER), Eq(0));
}

TEST_F(OrderStateTests, WhenNotEnoughItemsInStoreOrderShouldNotBeFilled)
{
    Order order(TALISKER, 51);

    order.fill(warehouse_);

    ASSERT_FALSE(order.is_filled());
    ASSERT_THAT(warehouse_.get_inventory(TALISKER), Eq(50));
}
