#ifndef WAREHOUSE_HPP
#define WAREHOUSE_HPP

class Warehouse
{
public:
    virtual void add(const std::string& product, size_t count) = 0;
    virtual size_t get_inventory(const std::string& product) const = 0;
    virtual bool has_inventory(const std::string& product, size_t quantity) const = 0;
    virtual void remove(const std::string& product, size_t quantity) = 0;
    virtual ~Warehouse() {}
};

#endif // WAREHOUSE_HPP
