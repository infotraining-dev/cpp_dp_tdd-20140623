#ifndef STATE_TEST_HPP
#define STATE_TEST_HPP

#include <iostream>
#include <string>
#include <map>
#include "gmock/gmock.h"

#include "warehouse.hpp"
#include "order.hpp"

class WarehouseImpl : public Warehouse
{
    std::map<std::string, size_t> inventory_;
public:
    virtual void add(const std::string& product, size_t count)
    {
        inventory_.insert(make_pair(product, count));
    }

    virtual size_t get_inventory(const std::string& product) const
    {
        auto where = inventory_.find(product);

        if (where != inventory_.end())
            return where->second;

        throw std::runtime_error("Key not found in map");
    }

    virtual bool has_inventory(const std::string& product, size_t quantity) const
    {
        auto where = inventory_.find(product);

        if (where != inventory_.end())
            return where->second >= quantity;

        return false;
    }

    virtual void remove(const std::string& product, size_t quantity)
    {
        inventory_[product] -= quantity;
    }
};

#endif
