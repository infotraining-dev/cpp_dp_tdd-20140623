#ifndef BEHAVIOR_TEST_HPP
#define BEHAVIOR_TEST_HPP

#include "gmock/gmock.h"

#include "warehouse.hpp"
#include "order.hpp"


class WarehouseMock : public Warehouse
{
public:
    MOCK_METHOD2(add, void (const std::string&, size_t));
    MOCK_CONST_METHOD1(get_inventory, size_t (const std::string&));
    MOCK_CONST_METHOD2(has_inventory, bool (const std::string&, size_t));
    MOCK_METHOD2(remove, void (const std::string&, size_t));
};

#endif // BEHAVIOR_TEST_HPP
